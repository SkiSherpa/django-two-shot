from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
    )


class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
