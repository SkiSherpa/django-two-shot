from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm
from django.contrib.auth.decorators import login_required


# Create your views here.
# get_all_receipts will create an instances of the Receipt model,
# and place them in soome context and render the html
@login_required
def get_all_receipts(req):
    receipts = Receipt.objects.filter(purchaser=req.user)
    context = {
        "all_receipts": receipts,
    }
    return render(req, "receipts/list.html", context)


@login_required
def create_receipt(req):
    if req.method == "POST":
        form = ReceiptForm(req.POST)
        if form.is_valid():
            # don't save to db yet, b/c we want to modify receipt.purchaser
            receipt = form.save(False)
            # change the purchaser to the current user,
            # b/c the db is expecting a purchaser
            receipt.purchaser = req.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}

    return render(req, "receipts/create.html", context)


@login_required
def get_expenses(req):
    expenses = ExpenseCategory.objects.filter(owner=req.user)
    context = {
        "expenses": expenses,
    }
    print(context)
    return render(req, "receipts/categories.html", context)


@login_required
def get_accounts(req):
    accounts = Account.objects.filter(owner=req.user)
    context = {
        "accounts": accounts,
    }
    return render(req, "receipts/accounts.html", context)
