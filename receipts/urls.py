from django.urls import path
from receipts.views import (
    get_all_receipts,
    create_receipt,
    get_expenses,
    get_accounts,
)

urlpatterns = [
    path("", get_all_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", get_expenses, name="category_list"),
    path("accounts/", get_accounts, name="account_list"),
]
