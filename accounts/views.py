from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


# Create your views here.
# display LogInForm form when it is a POST method
def user_login(req):
    if req.method == "POST":
        form = LogInForm(req.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                req,
                username=username,
                password=password,
            )
            if user is not None:
                login(req, user)
                return redirect("home")
    else:
        form = LogInForm()
    context = {
        "form": form,
    }
    return render(req, "accounts/login.html", context)


def user_logout(req):
    logout(req)
    return redirect("login")


def user_signup(req):
    # user would have clicked a sign_up button
    if req.method == "POST":
        # pull SignUpForm framework from accounts.forms
        signup_form = SignUpForm(req.POST)
        if signup_form.is_valid():
            username = signup_form.cleaned_data["username"]
            password = signup_form.cleaned_data["password"]
            password_confirmation = signup_form.cleaned_data[
                "password_confirmation"
            ]
            # if passwords match
            if password == password_confirmation:
                # create a user for vals typed above
                user = User.objects.create_user(
                    username,
                    password,
                )
                # use built-in login fn, argus (req, user)
                login(req, user)
                return redirect("home")
            # IF passwords don't match, error saying so
            else:
                signup_form.add_error("passwords do not match")
    # unclear as to why this is here - raw fields with no IP from user, empty
    # form
    else:
        # When blank, there is a hidden req.GET, cause you need to the form to
        # do anything, sign up
        signup_form = SignUpForm()

    # You want the html page empty when someone is going to sign up
    context = {
        "signup_form": signup_form,
    }
    # This render never runs when the "signup" button is clicked
    return render(req, "accounts/signup.html", context)
